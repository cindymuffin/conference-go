import json
import requests
from .keys import PEXELS_API_KEY


def get_picture(search_city):
    url = "https://api.pexels.com/v1/search?per_page=1&query=" + search_city
    headers = {"Authorization": "Bearer " + PEXELS_API_KEY}
    response = requests.get(url, headers=headers)
    data = json.loads(response.content)
    photos = data["photos"]
    return photos
